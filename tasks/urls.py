from django.urls import path
from tasks.views import create_task, show__my_tasks


urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", show__my_tasks, name="show_my_tasks"),
]
